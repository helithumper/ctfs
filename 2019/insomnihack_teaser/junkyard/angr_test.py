import angr
proj = angr.Project('junkyard', auto_load_libs=False)
state = proj.factory.entry_state()
simgr = proj.factory.simgr(state)

simgr.explore()

print(simgr.errored)