import requests


headers = {
    "User-Agent": "curl/7.54.0"
}

https_proxy = "https://localhost:8888"

proxyDict = { 
              "https" : https_proxy
            }


start = "https://curlpipebash.teaser.insomnihack.ch/print-flag.sh"

while (True):
    print("[*] Attempting: " + start)
    r = requests.get(start, headers=headers)
    if('insomnihack' not in r.text):
        print("[-] Failure. Trying Again")
        continue
    
    try:
        start = r.text.split(' ')[2]
    except:
        print(type(r.text))
        print(r.text)
        print("Couldn't Split: " + r.text)