# Important Addresses
*Beginning of Algo*: 0x55555555a78b

Bottom of First Iteration through string
```[----------------------------------registers-----------------------------------]
RAX: 0x7ffff6c21018 --> 0x41 ('A')
RBX: 0x1 
RCX: 0x18 
RDX: 0x7ffff6c0db88 --> 0x0 
RSI: 0x1 
RDI: 0x7ffff6c21018 --> 0x41 ('A')
RBP: 0x1 
RSP: 0x7fffffffdcf0 --> 0x7ffff6c25050 --> 0x1 
RIP: 0x55555555a90d (<_ZN15beginer_reverse4main17h80fa15281f646bc1E+621>:	jne    0x55555555a870 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+464>)
R8 : 0x1 
R9 : 0x8080808080808080 
R10: 0xa ('\n')
R11: 0x246 
R12: 0x41 ('A')
R13: 0x7ffff6c20040 ("AAAABBBBCCCCDDDDEEEEFFFF\n")
R14: 0x7ffff6c21018 --> 0x41 ('A')
R15: 0x4
EFLAGS: 0x206 (carry PARITY adjust zero sign trap INTERRUPT direction overflow)
[-------------------------------------code-------------------------------------]
   0x55555555a901 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+609>:	add    rbp,0x1
   0x55555555a905 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+613>:	mov    QWORD PTR [rsp+0x30],rbp
   0x55555555a90a <_ZN15beginer_reverse4main17h80fa15281f646bc1E+618>:	cmp    rcx,rbp
=> 0x55555555a90d <_ZN15beginer_reverse4main17h80fa15281f646bc1E+621>:	jne    0x55555555a870 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+464>
 | 0x55555555a913 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+627>:	jmp    0x55555555a91f <_ZN15beginer_reverse4main17h80fa15281f646bc1E+639>
 | 0x55555555a915 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+629>:	mov    r14d,0x4
 | 0x55555555a91b <_ZN15beginer_reverse4main17h80fa15281f646bc1E+635>:	xor    ebx,ebx
 | 0x55555555a91d <_ZN15beginer_reverse4main17h80fa15281f646bc1E+637>:	xor    ebp,ebp
 |->   0x55555555a870 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+464>:	movzx  r12d,BYTE PTR [r13+rbp*1+0x0]
       0x55555555a876 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+470>:	mov    rbx,rsi
       0x55555555a879 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+473>:	cmp    rbp,rsi
       0x55555555a87c <_ZN15beginer_reverse4main17h80fa15281f646bc1E+476>:	jne    0x55555555a8fd <_ZN15beginer_reverse4main17h80fa15281f646bc1E+605>
                                                                  JUMP is taken
[------------------------------------stack-------------------------------------]
0000| 0x7fffffffdcf0 --> 0x7ffff6c25050 --> 0x1 
0008| 0x7fffffffdcf8 --> 0x7ffff6c20040 ("AAAABBBBCCCCDDDDEEEEFFFF\n")
0016| 0x7fffffffdd00 --> 0x19 
0024| 0x7fffffffdd08 --> 0x18 
0032| 0x7fffffffdd10 --> 0x7ffff6c21018 --> 0x41 ('A')
0040| 0x7fffffffdd18 --> 0x1 
0048| 0x7fffffffdd20 --> 0x1 
0056| 0x7fffffffdd28 --> 0x18 
[------------------------------------------------------------------------------]
```


[-------------------------------------code-------------------------------------]
   0x55555555a91f <_ZN15beginer_reverse4main17h80fa15281f646bc1E+639>:	lea    rax,[rbp*4+0x0]
   0x55555555a927 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+647>:	xor    ecx,ecx
   0x55555555a929 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+649>:	nop    DWORD PTR [rax+0x0]
=> 0x55555555a930 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+656>:	cmp    rax,rcx
   0x55555555a933 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+659>:	je     0x55555555a962 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+706>
   0x55555555a935 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+661>:	mov    edx,DWORD PTR [r14+rcx*1]
   0x55555555a939 <_ZN15beginer_reverse4main17h80fa15281f646bc1E+665>:	add    edx,0xffffffe0
   0x55555555a93c <_ZN15beginer_reverse4main17h80fa15281f646bc1E+668>:	add    rcx,0x4
[------------------------------------stack-------------------------------------]
