# PlaidCTF 2019: Everland

## Description

```Plaintext
Everland
Misc (250 pts)
In a darkened land, a hero must fight for their flag! Source
running at everland.pwni.ng:7772
```

## First Thoughts and incorrect languages

To be honest, I began this challenge thinking the code was written in SMALI due to the `.sml` extension at the end of the source file name. I don’t have much experience with SMALI and as such didn’t know what It looked like. After some looking into SMALI and the files associated, as well as some convincing by teammates, we ended up figuring out that the challenge was actually [SML](http://www.smlnj.org/). After we discovered that the file was SML, we were actually able to grasp what was going on with it rather quickly.

## The Game

The challenge is a basic RPG game where a user is trying to fight a set of monsters. How many monsters? 10.

```sml
  (* Hey, count yourself lucky. I could have made it 100 *)
  val num_enemies = 10
```

So we started up the game and began playing a bit.

```sml
$ nc everland.pwni.ng 7772
Welcome, mighty hero, to the abyss of Everland. Pray tell, what is your name? user

Well, user. It is valiant of you to venture here, but Everland is a place of darkness and despair.
Few adventurers have dared descend these halls, and fewer have returned alive.
Good luck, and godspeed.


user (100st) [||||||||||] -- Count Dorkula (50st): 80hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  > fight
Count Dorkula used Spear Lunge!

user (100st) [||||||    ] -- Count Dorkula (50st): 70hp

Available Moves:
  - (0) Sword Strike
  - (1) Full Empower
  - (2) Recouperate
  - (3) Skip Turn

> 0
Using: Sword Strike

user (100st) [||||||    ] -- Count Dorkula (50st): 55hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  > forage
Collected Gross Weed!

user (100st) [||||||    ] -- Count Dorkula (50st): 55hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  > use
Available Items:
  - (0) Gross Weed
  - (1) Max Health Potion
  - (2) Sharpened Dagger

> 0
Using: Gross Weed

user (100st) [||||||    ] -- Count Dorkula (50st): 55hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  >
```

So we know a few things off the bat:
    1. We have three options per turn: `fight`,`forage`, and `use`.
    2. Fight gives us attack options
    3. Forage gives us the ability to find different items in our environment
    4. Use lets us use items we have found
    5. Both our character and the enemies have names, health values, and strength counters

So we then began looking in the code for a possible way to win the game. How did we know that we needed to win the game to get the flag? It’s in the code!

```sml
fun play_game hero Flag world _ =
  (TextIO.print ("Wow, looks like you beat all the enemies!\n"
                ^"Guess you deserve a flag: "); raise (CatFlag (get_flag())))
```

So then we spent a while trying to figure out what each attack did. This was our general table of attack definitions:

```sml
Attacks:
Kill (Death Wave)- automatically kills you
Lunge (Spear Lunge)- your health goes to 2/3 what you currently have, the attacker loses 10 health
Strike (Sword Strike)- Target loses 15 health
Empower (Full Empower)- User loses 20 health and gains 20 S
Recoup (Recuperate)- -20 S, +10 health (doesn't check that you have enough S)
Capture (Capture)- ?
Stink (Stink Out)- Target S - 10
Wrap (Vine Wrap)- -5 S to target, -5 health to target
```

So our first vulnerability in the game was the ability to Recuperate.  This move (thankfully) didn’t check if you had enough strength to actually use it. As such, we could use it as much as we want.

```sml
fun recoup_fn (my_h, my_s, their_h, their_s) =
let
  val mh = !my_h
  val ms = !my_s
in
  (fn () => (my_s := max(!my_s - 20, 0); my_h := (!my_h + 10)),
   fn () => (my_s := ms; my_h := mh))
end
```

The bug is that the function will decrement our strength until it hits 0, but it won’t keep us from healing if our strength is less than 20. So we now have an effective way to heal ourselves. Now this may sound a bit ineffective as we aren’t dealing any damage; However, we don’t have to deal damage to the enemy if they’re damaging themselves. Remember the Pokemon move `confusion`?

```sml
(*Lunge attack*)
fun lunge_fn (my_h, my_s, their_h, their_s) =
let
  val th = !their_h
  val mh = !my_h
in
  (fn () => (their_h := 2*(!their_h) div 3; my_h := max(!my_h-10, 0)),
   fn () => (their_h := th; my_h := mh))
end
```

As such, the enemy will attack us and decrease our health by 10, but at the same time they will be dropping 1/3 of their health each time they use the attack. So all we need to do to get through enemies is recuperate a bunch? Right? That would work except for the final boss…

```sml
Not_a_winner (0st) [|         ] -- Posessed Count Dorkula (250st): 250hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  > $ fight
An eerie wind rushes past, and a fallen foe rises again...


Not_a_winner (0st) [|         ] -- Posessed Count Dorkula (250st): 250hp

Available Moves:
  - (0) Sword Strike
  - (1) Full Empower
  - (2) Recouperate
  - (3) Skip Turn
  - (4) Sacrifice

> $ 2
Using: Recouperate

Not_a_winner (0st) [||        ] -- Posessed Count Dorkula (250st): 250hp

Are you ready for your next fight? You can:
  - fight
  - forage
  - use
  > $ fight
Posessed Count Dorkula used Death Wave!
GameOver: You Died!
And thus, another passes to dust...
```

The 11th enemy we face is a possessed enemy. What does this mean? Not only does the possessed enemy have a much higher strength and health pool, they also only have a single attack: `Death Wave`. Let’s look at the code for `Death Wave`:

```sml
(* Kills the opponent*)
fun kill_fn (my_h, my_s, their_h, their_s) =
let
  val h = !their_h
in
  (* It kills you. You're dead *)
  ((fn () => their_h := 0), (fn () => their_h := h))
end
```

!["suprised"](https://gifimage.net/wp-content/uploads/2017/09/anime-surprised-gif-4.gif )

So the possessed enemy can insta-kill us on the second turn… Great…. But at least we get one turn to do something! So we must figure out what that something is! Let’s look through some of our attacks again. We had an attack called `Sacrifice` that we didn’t look into much earlier.

```sml
fun sacrifice_fn (my_h, my_s, their_h, their_s) =
          (fn () => (
             my_h := min((!my_h)+min(!e_h, !my_s*10), player_max);
             e_h  := (!e_h-(!my_h)*10);
             p_ms := List.filter (fn (n, _) => n <> "Sacrifice") (!p_ms)),
           fn () => ()) (* Only used by the AI, not us *)
        val _ = p_ms := (List.filter (fn (n, _) => n <> "Capture") (!p_ms))
                        @[("Sacrifice", sacrifice_fn)]
```

Oh, there’s a slight issue here that makes this attack a bit imbalanced. Let’s look at the 3rd and 4th line:

```sml
my_h := min((!my_h)+min(!e_h, !my_s*10), player_max);
             e_h  := (!e_h-(!my_h)*10);
```

Let’s plug in the values at the end here and double check this:

```sml
Winner (0st) [||        ] -- Posessed Count Dorkula (250st): 250hp
```

So we have 20hp (each bar is 20%), the enemy has 250hp

```sml
my_h := min((40)+min(250, 0), 200);  (* my_h ends up being 40*)
e_h  := (250-(40)*10); (* e_h ends up being -150*)
```

So there we go, we just have to sacrifice while battling the possessed creature. In order for this to work, we decided to get the `Sacrificial Net` while on the first enemy, capture the first enemy, iterate through the other enemies by using our recuperate loop strategy, then sacrifice our captured friend while battling the final boss. As such, we will win the battle!
And once we run the `win.py` file, we get the flag:

```sml
[+] Opening connection to everland.pwni.ng on port 7772: Done
[STEP 1] Obtaining Net
[STEP 2] Bring enemy down in health to sub-50
reducing health: Elektrikal [70hp]
reducing health: Elektrikal [60hp]
reducing health: Elektrikal [50hp]
reducing health: Elektrikal [50hp]
reducing health: Elektrikal [50hp]
reducing health: Elektrikal [40hp]
[STEP 3] Capture the enemy
Item's spot in the inventory: 1
[STEP 4] Get to final boss
[ROUND 1] -->                 Spatula (50st): 80hp
[ROUND 1] -->                 Spatula (50st): 70hp
[ROUND 1] -->                 Spatula (50st): 60hp
[ROUND 1] -->                 Spatula (50st): 50hp
[ROUND 1] -->                 Spatula (50st): 40hp
[ROUND 1] -->                 Spatula (50st): 30hp
[ROUND 1] -->                 Spatula (50st): 20hp
[ROUND 1] -->                 Spatula (50st): 10hp
[ROUND 1] Killed Enemy
[ROUND 2] -->           Count Dorkula (50st): 80hp
[ROUND 2] -->           Count Dorkula (50st): 70hp
[ROUND 2] -->           Count Dorkula (50st): 60hp
[ROUND 2] -->           Count Dorkula (50st): 50hp
[ROUND 2] -->           Count Dorkula (50st): 40hp
[ROUND 2] -->           Count Dorkula (50st): 30hp
[ROUND 2] -->           Count Dorkula (50st): 20hp
[ROUND 2] -->           Count Dorkula (50st): 10hp
[ROUND 2] Killed Enemy
[ROUND 3] -->          Skedankedanker (50st): 80hp
[ROUND 3] -->          Skedankedanker (50st): 70hp
[ROUND 3] -->          Skedankedanker (50st): 60hp
[ROUND 3] -->          Skedankedanker (50st): 50hp
[ROUND 3] -->          Skedankedanker (50st): 40hp
[ROUND 3] -->          Skedankedanker (50st): 30hp
[ROUND 3] -->          Skedankedanker (50st): 20hp
[ROUND 3] -->          Skedankedanker (50st): 10hp
[ROUND 3] Killed Enemy
[ROUND 4] -->              Elektrikal (50st): 80hp
[ROUND 4] -->              Elektrikal (50st): 70hp
[ROUND 4] -->              Elektrikal (50st): 60hp
[ROUND 4] -->              Elektrikal (50st): 50hp
[ROUND 4] -->              Elektrikal (50st): 40hp
[ROUND 4] -->              Elektrikal (50st): 30hp
[ROUND 4] -->              Elektrikal (50st): 20hp
[ROUND 4] -->              Elektrikal (50st): 10hp
[ROUND 4] Killed Enemy
[ROUND 5] -->                 Spatula (50st): 80hp
[ROUND 5] -->                 Spatula (50st): 70hp
[ROUND 5] -->                 Spatula (50st): 60hp
[ROUND 5] -->                 Spatula (50st): 50hp
[ROUND 5] -->                 Spatula (50st): 40hp
[ROUND 5] -->                 Spatula (50st): 30hp
[ROUND 5] -->                 Spatula (50st): 20hp
[ROUND 5] -->                 Spatula (50st): 10hp
[ROUND 5] Killed Enemy
[ROUND 6] -->           Count Dorkula (50st): 80hp
[ROUND 6] -->           Count Dorkula (50st): 70hp
[ROUND 6] -->           Count Dorkula (50st): 60hp
[ROUND 6] -->           Count Dorkula (50st): 50hp
[ROUND 6] -->           Count Dorkula (50st): 40hp
[ROUND 6] -->           Count Dorkula (50st): 30hp
[ROUND 6] -->           Count Dorkula (50st): 20hp
[ROUND 6] -->           Count Dorkula (50st): 10hp
[ROUND 6] Killed Enemy
[ROUND 7] -->          Skedankedanker (50st): 80hp
[ROUND 7] -->          Skedankedanker (50st): 70hp
[ROUND 7] -->          Skedankedanker (50st): 60hp
[ROUND 7] -->          Skedankedanker (50st): 50hp
[ROUND 7] -->          Skedankedanker (50st): 40hp
[ROUND 7] -->          Skedankedanker (50st): 30hp
[ROUND 7] -->          Skedankedanker (50st): 20hp
[ROUND 7] -->          Skedankedanker (50st): 10hp
[ROUND 7] Killed Enemy
[ROUND 8] -->              Elektrikal (50st): 80hp
[ROUND 8] -->              Elektrikal (50st): 70hp
[ROUND 8] -->              Elektrikal (50st): 60hp
[ROUND 8] -->              Elektrikal (50st): 50hp
[ROUND 8] -->              Elektrikal (50st): 40hp
[ROUND 8] -->              Elektrikal (50st): 30hp
[ROUND 8] -->              Elektrikal (50st): 20hp
[ROUND 8] -->              Elektrikal (50st): 10hp
[ROUND 8] Killed Enemy
[ROUND 9] -->                 Spatula (50st): 80hp
[ROUND 9] -->                 Spatula (50st): 70hp
[ROUND 9] -->                 Spatula (50st): 60hp
[ROUND 9] -->                 Spatula (50st): 50hp
[ROUND 9] -->                 Spatula (50st): 40hp
[ROUND 9] -->                 Spatula (50st): 30hp
[ROUND 9] -->                 Spatula (50st): 20hp
[ROUND 9] -->                 Spatula (50st): 10hp
[ROUND 9] Killed Enemy
[STEP 5] Sacrifice to Win
########################################################
## PCTF{just_be_glad_i_didnt_arm_cpt_hook_with_GADTs} ##
########################################################
[*] Closed connection to everland.pwni.ng port 7772
```
