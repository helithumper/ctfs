# Chaingineering

- 250pts
- PWN

## Description

```
Solve WebKid and Pillow first, then combine them into
a single exploit and read /flag2 :)

Download the challenge files from here and submit your exploits here.

Difficulty estimate: easy
```

## Links

[Challenge Files](https://35c3ctf.ccc.ac/uploads/WebKid-7a2c78814764c77b3b8e1d8391b9cabcb2a58810.zip)
