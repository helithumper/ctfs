#!/usr/bin/python
from pwn import *
r = remote('35.207.157.79', 4444)
def pow():
    r.recvuntil('./pow.py ')
    seed = r.recvline().rstrip()
    log.info('Seed: {}'.format(seed))
    
    e = process(['../../pow.py', seed])

    line = ""
    while "Solution" not in line:
        line = e.recvline()
        log.info(line)

    log.info(line)
    proof = line.split(": ")[1]
    r.recvuntil('response? ')
    r.sendline(proof)
def exploit():
    pow()
    r.interactive()
exploit()
